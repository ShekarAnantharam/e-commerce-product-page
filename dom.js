const minus = document.getElementById('minus');
const plus = document.getElementById('plus');
const cart = document.getElementById("cart");
const emptyCart = document.getElementById("display-empty-cart");
const filledCart = document.getElementById("display-filled-cart");
const hamburger=document.getElementById('hamb');
const addToCart=document.getElementById("click-to-add");
const deleteItems=document.getElementById("delete-icon")
const closeMenu = document.getElementById("close-icon");
const mainImage = document.getElementById('main-image');
const rightArrow = document.getElementById('right-arrow');
const leftArrow = document.getElementById("left-arrow");
const displayCartQuantity=document.getElementById("cart-quantity");
const cartQuantity=document.getElementById("cart-quantity-background");
const product=document.getElementById("product-view");
const lightBox=document.getElementById("light-box");
const thumbnail1=document.getElementById("thumbnail-1");
const thumbnail2=document.getElementById("thumbnail-2");
const thumbnail3=document.getElementById("thumbnail-3");
const thumbnail4=document.getElementById("thumbnail-4");
const lightBoxThumbnail1=document.getElementById("light-box-thumbnail-1");
const lightBoxThumbnail2=document.getElementById("light-box-thumbnail-2");
const lightBoxThumbnail3=document.getElementById("light-box-thumbnail-3");
const lightBoxThumbnail4=document.getElementById("light-box-thumbnail-4");
const lightBoxCloseIcon=document.getElementById("light-box-close-icon")
const lightBoxProduct=document.getElementById("light-box-product-view");
const lightBoxRightArrow = document.getElementById('light-box-right-arrow');
const lightBoxLeftArrow = document.getElementById("light-box-left-arrow");

thumbnail1.style.hover="cursor:pointer";
thumbnail2.style.hover="cursor:pointer";
thumbnail3.style.hover="cursor:pointer";
thumbnail4.style.hover="cursor:pointer opacity:0.5";
lightBoxThumbnail1.style.hover="cursor:pointer";
lightBoxThumbnail2.style.hover="cursor:pointer";
lightBoxThumbnail3.style.hover="cursor:pointer";
lightBoxThumbnail4.style.hover="cursor:pointer";
thumbnail1.style.border="3px solid hsl(26, 100%, 55%)";
thumbnail1.style.opacity="0.5";
lightBoxThumbnail1.style.border="3px solid hsl(26, 100%, 55%)";
lightBoxThumbnail1.style.opacity="0.5";
// lightBoxCloseIcon.style.hover="cursor:pointer";

// console.log(product.innerHTML);
displayCartQuantity.style.display="none";


let navigate=document.getElementById('menu');
// console.log(navigate);
// const hamburger=document.getElementById('hamb');
// console.log(hamburger);
minus.addEventListener("click",decrease);
plus.addEventListener("click", increase);
cart.addEventListener("click", showCart);
hamburger.addEventListener("click",navMenu);
addToCart.addEventListener("click", addCart);
deleteItems.addEventListener("click", deleteProduct);
closeMenu.addEventListener("click", closeTheMenu);
rightArrow.addEventListener("click" ,displayNextImage);
leftArrow.addEventListener("click" ,displayPreviousImage);
product.addEventListener("click", displayLightBox);
thumbnail1.addEventListener("click",displayProduct);
thumbnail2.addEventListener("click",displayProduct);
thumbnail3.addEventListener("click",displayProduct);
thumbnail4.addEventListener("click",displayProduct);
lightBoxCloseIcon.addEventListener("click", closeLightBox);
lightBoxThumbnail1.addEventListener("click",displayLightBoxProduct);
lightBoxThumbnail2.addEventListener("click",displayLightBoxProduct);
lightBoxThumbnail3.addEventListener("click",displayLightBoxProduct);
lightBoxThumbnail4.addEventListener("click",displayLightBoxProduct);
lightBoxRightArrow.addEventListener("click" ,displayLightBoxNextImage);
lightBoxLeftArrow.addEventListener("click" ,displayLightBoxPreviousImage);

// let quantity=0;
let quantity=0;

function  decrease(){
    quantity=Number(document.getElementById("count").innerText);
    if(quantity>0){
        quantity-=1;
        minus.nextElementSibling.innerText=quantity;
    }
}
function increase(){
    quantity=Number(document.getElementById("count").innerText);
    quantity+=1;
    emptyCart.style.display="none";

    // document.getElementById('count').innerHTML=quantity;
    // console.log(plus.previousElementSibling);
    plus.previousElementSibling.innerText=quantity;
    // console.log(document.getElementById("count"));

    // let count2=document.getElementById('çount');
    // let num=Number(count2.innerText)
    // count2.innerText=num+1;

}
emptyCart.style.display="none";
filledCart.style.display="none";
let addToCartClickCount=0;
// const displayPrice=document.getElementById('filled-pricing');
// const totalPrice=document.createElement('p');
const displayPrice=document.getElementById('display-price');




function showCart(){
    quantity=Number(document.getElementById("count").innerText);
    // let emptyCart=displayCart.children;
    // console.log(emptyCart);
    // console.log(displayPrice);
// emptyCart.style.display="block";

    if(quantity==0){
        if(emptyCart.style.display=="none"){
            emptyCart.style.display="block";

        }
        else if(emptyCart.style.display=="block"){
        emptyCart.style.display="none";

        }
    }  
    else if(quantity>0 & addToCartClickCount>0){
        emptyCart.style.display="none";
        
        if(filledCart.style.display=="block"){
            filledCart.style.display= "none";
        }
        else if(filledCart.style.display= "none"){
            filledCart.style.display= "block";
        }
        } 
        else{
            if(emptyCart.style.display=="none"){
                emptyCart.style.display="block";
    
            }
            else if(emptyCart.style.display=="block"){
            emptyCart.style.display="none";
    
            }

        }   
}
function addCart(e){
    addToCartClickCount+=1;
    quantity=Number(document.getElementById("count").innerText);
    if(quantity==0){
        displayCartQuantity.style.display="none";
    }
    else{
    let amount=quantity*125;
    displayPrice.innerText=`$125.00 x ${quantity} $${amount}`;
    cartQuantity.innerText=quantity;
    displayCartQuantity.style.display="block";
    }
    
// console.log(displayPrice);
}
// function navMenu(){
//     console.log(navigate);
//     displayCart.style.display="block";
    
// }
function navMenu(){
  
    navigate.style.display="block";
   
    
}
function deleteProduct(){
    console.log(deleteItems);
    filledCart.style.display= "none";
    emptyCart.style.display="block";
    plus.previousElementSibling.innerText=0;
    displayCartQuantity.style.display="none";

}
function closeTheMenu(){
    navigate.style.display="none";

}
let image=1;
function displayNextImage(){
    image+=1;
    if(image>4){
        image=1;
        mainImage.style.backgroundImage=`url(./../images/image-product-${image}.jpg)`;
    }
    else{
        mainImage.style.backgroundImage=`url(./../images/image-product-${image}.jpg)`;
    }
}
function displayPreviousImage(){
    image-=1;
    if(image<=0){
        image=4;
        mainImage.style.backgroundImage=`url(./../images/image-product-${image}.jpg)`;
    }
    else{
        mainImage.style.backgroundImage=`url(./../images/image-product-${image}.jpg)`;
    }
}
function displayLightBox(){
    lightBox.style.display="block";
}
let productNumber=0;
let previousThumbnail=null, thumbnailClick=0;
function displayProduct(event){
    thumbnail1.style.border="none";
thumbnail1.style.opacity="1";
    thumbnailClick+=1;
    let target=event.target.id;
    target=[...target];
    //  console.log(event.target.parentElement.);
     for(let i=0;i<target.length;i++){
         if(i==target.length-1){
             productNumber=target[i];
         }
     }
    //  console.log(typeof Number(productNumber));
     product.innerHTML=`<img src="./images/image-product-${productNumber}.jpg" alt=""></img>`
    if (thumbnailClick==1){
        event.target.style.border="3px solid hsl(26, 100%, 55%)";
        event.target.style.opacity="0.5";

        previousThumbnail=event.target;
    }
    else if(thumbnailClick>1){
        previousThumbnail.style.border="none";
        previousThumbnail.style.opacity="1";
        event.target.style.border="3px solid hsl(26, 100%, 55%)";
        event.target.style.opacity="0.5";
        previousThumbnail=event.target;
        
    }

}
function closeLightBox(){
    lightBox.style.display="none";

}
let lightBoxProductNumber=0;
let lightBoxPreviousThumbnail=null, lightBoxThumbnailClick=0;
function displayLightBoxProduct(event){
    lightBoxThumbnail1.style.border="none";
    lightBoxThumbnail1.style.opacity="1";
    lightBoxThumbnailClick+=1;
    let target=event.target.id;
    target=[...target];
     console.log(event.target);
     for(let i=0;i<target.length;i++){
         if(i==target.length-1){
            lightBoxProductNumber=target[i];
         }
     }
    //  console.log(typeof Number(productNumber));
    lightBoxProduct.innerHTML=`<img src="./images/image-product-${lightBoxProductNumber}.jpg" alt=""></img>`
    if (lightBoxThumbnailClick==1){
        event.target.style.border="3px solid hsl(26, 100%, 55%)";
        event.target.style.opacity="0.5";

        lightBoxPreviousThumbnail=event.target;
    }
    else if(lightBoxThumbnailClick>1){
        lightBoxPreviousThumbnail.style.border="none";
        lightBoxPreviousThumbnail.style.opacity="1";
        event.target.style.border="3px solid hsl(26, 100%, 55%)";
        event.target.style.opacity="0.5";
        lightBoxPreviousThumbnail=event.target;
        
    }

}
let lightBoxImage=1;
function displayLightBoxNextImage(){
    lightBoxThumbnail1.style.border="none";
    lightBoxThumbnail1.style.opacity="1";
    // lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    lightBoxImage+=1;
    if(lightBoxImage>4){
        lightBoxImage=1;
        lightBoxProduct.innerHTML=`<img src="./images/image-product-${lightBoxImage}.jpg" alt=""></img>`
        // if(lightBoxImage==1){
        //     lightBoxPreviousThumbnail.style.border="none";
        // lightBoxPreviousThumbnail.style.opacity="1";
        //     lightBoxThumbnail1.style.border="3px solid hsl(26, 100%, 55%)";
        // lightBoxThumbnail1.style.opacity="0.5";
        // lightBoxPreviousThumbnail=event.target;
        // }
        // else if(lightBoxImage==2){
        //     lightBoxPreviousThumbnail.style.border="none";
        // lightBoxPreviousThumbnail.style.opacity="1";
        //     lightBoxThumbnail2.style.border="3px solid hsl(26, 100%, 55%)";
        // lightBoxThumbnail2.style.opacity="0.5";
        // lightBoxPreviousThumbnail=event.target;
        // }
        // else if(lightBoxImage==3){
        //     lightBoxPreviousThumbnail.style.border="none";
        // lightBoxPreviousThumbnail.style.opacity="1";
        //     lightBoxThumbnail3.style.border="3px solid hsl(26, 100%, 55%)";
        // lightBoxThumbnail3.style.opacity="0.5";
        // lightBoxPreviousThumbnail=event.target;
        // }
        // else if(lightBoxImage==4){
        //     lightBoxPreviousThumbnail.style.border="none";
        // lightBoxPreviousThumbnail.style.opacity="1";
        //     lightBoxThumbnail4.style.border="3px solid hsl(26, 100%, 55%)";
        // lightBoxThumbnail4.style.opacity="0.5";
        // lightBoxPreviousThumbnail=event.target;
        // }
    }
    else{
    lightBoxProduct.innerHTML=`<img src="./images/image-product-${lightBoxImage}.jpg" alt=""></img>`
    // if(lightBoxImage==1){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail1.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail1.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    // else if(lightBoxImage==2){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail2.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail2.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    // else if(lightBoxImage==3){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail3.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail3.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    // else if(lightBoxImage==4){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail4.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail4.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    }
}
function displayLightBoxPreviousImage(){
    lightBoxThumbnail1.style.border="none";
    lightBoxThumbnail1.style.opacity="1";
    // lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    lightBoxImage-=1;
    if(lightBoxImage<=0){
        lightBoxImage=4;
    lightBoxProduct.innerHTML=`<img src="./images/image-product-${lightBoxImage}.jpg" alt=""></img>`
    // if(lightBoxImage==1){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail1.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail1.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    // else if(lightBoxImage==2){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail2.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail2.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    // else if(lightBoxImage==3){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail3.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail3.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    // else if(lightBoxImage==4){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail4.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail4.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    }
    else{
    lightBoxProduct.innerHTML=`<img src="./images/image-product-${lightBoxImage}.jpg" alt=""></img>`
    // if(lightBoxImage==1){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail1.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail1.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    // else if(lightBoxImage==2){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail2.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail2.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    // else if(lightBoxImage==3){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail3.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail3.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    // else if(lightBoxImage==4){
    //     lightBoxPreviousThumbnail.style.border="none";
    //     lightBoxPreviousThumbnail.style.opacity="1";
    //     lightBoxThumbnail4.style.border="3px solid hsl(26, 100%, 55%)";
    // lightBoxThumbnail4.style.opacity="0.5";
    // lightBoxPreviousThumbnail=event.target;
    // }
    }
}